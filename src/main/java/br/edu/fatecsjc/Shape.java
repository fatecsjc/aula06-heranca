package br.edu.fatecsjc;

public abstract class Shape {

    public abstract double getArea();

    public abstract String toString();
}
